import VueRouter from 'vue-router'

import HomeContainer from './subcom/tabbar/HomeContainer.vue'
import MemberContainer from './subcom/tabbar/MemberContainer.vue'
import SearchContainer from './subcom/tabbar/SearchContainer.vue'
import CartContainer from './subcom/tabbar/CartContainer.vue'
import newsList from './subcom/news/newsList.vue'
import newsInfo from './subcom/news/newsInfo.vue'
import PhotoList from './subcom/photo/PhotoList.vue'
import Photoinfo from './subcom/photo/Photoinfo.vue'
import Goodslist from './subcom/goods/GoodsList.vue'
import GoodsInfo from './subcom/goods/GoodsInfo.vue'
import GoodsDesc from './subcom/goods/GoodsDesc.vue'
import GoodsComs from './subcom/goods/GoodsComs.vue'


var router = new VueRouter({
    routes:[
        { path: '/', redirect: '/home'},
        { path: '/home', component: HomeContainer },
        { path: '/member', component: MemberContainer },
        { path: '/search', component: SearchContainer },
        { path: '/cart', component: CartContainer },
        { path:'/home/newslist', component: newsList},
        { path:'/home/newsinfo/:id', component: newsInfo },
        { path:'/home/photolist', component: PhotoList },
        { path: '/home/photoinfo/:pid', component: Photoinfo },
        { path: '/home/goodslist', component: Goodslist },
        { path: '/home/goodsinfo/:gid', component: GoodsInfo },
        { path: '/home/goodsdesc/:gid', component: GoodsDesc, name: 'goodsDesc' },
        { path: '/home/goodscoms/:gid', component: GoodsComs, name: 'goodsComs' }
    ],
    linkActiveClass: 'mui-active'
})

export default router