//项目js的入口
import Vue from 'vue'
//1.1 导入路由的包
import VueRouter from 'vue-router'
//1.2 安装路由
Vue.use(VueRouter)

// 导入格式化时间的插件
import moment from 'moment'
//注册全局过滤器
Vue.filter('dateFormat', function (dataStr, pattern='YYYY-MM-DD hh:mm:ss') {
    return moment(dataStr).format(pattern)
})

//导入mint-ui组件
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(Mint)

//导入MUI
import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

//导入vue-preview
import VuePreview from 'vue-preview'
Vue.use(VuePreview)

//导入vuex
import Vuex from 'vuex'
Vue.use(Vuex)

//在页面加载前 获取本地cart数据
let cart = JSON.parse(localStorage.getItem('cart') || '[]')

const store = new Vuex.Store({
    state:{
        cart: cart
    },
    mutations:{
        addToCart(state,goodsCount){
            //此时有两种情况：一是count已经有这个商品对象了，此时只需要改变商品数量即可；否则push到count中去
            let flag = false
            state.cart.some(item => {
                if(item.id === goodsCount.id){
                    item.count += goodsCount.count
                    flag = true
                    return true
                }
            })

            if(!flag){
                state.cart.push(goodsCount)
            }
            //本地持久化存储 cart
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        updateCart(state,goodsCount){
            state.cart.some(item => {
                if(item.id === goodsCount.id){
                    item.count = goodsCount.count
                    return true
                }
            })

            //本地持久化存储 cart
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        updateSelected(state, goods){
            state.cart.some(item => {
                if(item.id === goods.id){
                    item.selected = goods.selected
                    return true
                }
            })

            //本地持久化存储 cart
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        deleteCart(state, id){
            state.cart.some((item, i) => {
                if(item.id === id){
                    state.cart.splice(i, 1)
                    return true
                }
            })

            //本地持久化存储 cart
            localStorage.setItem('cart', JSON.stringify(state.cart))
        }
    },
    getters:{
        getAllCount(state){
            let c = 0
            state.cart.forEach(item => {
                c += item.count
            })
            return c;
        },
        getSelected(state){
            const o = {
                count: 0,
                amount: 0
            }
            state.cart.forEach(item => {
                if(item.selected){
                    o.count += item.count
                    o.amount += item.count * item.price
                }
            })

            return o
        }
    }
})

//导入app模块
import app from './App.vue'

//1.3 导入router对象
import router from './router.js'




var vue = new Vue({
    el:'#app',
    render: c => c(app),
    router,
    store
})