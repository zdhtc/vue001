const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
//vue-loader@15.x以上 需要加载VueLoaderPlugin
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
    mode: 'development',
    entry: path.join(__dirname, './src/main.js'),
    output:{
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js'
    },
    plugins:[
        new htmlWebpackPlugin({
            template: path.join(__dirname, './src/index.html'),
            filename: 'index.html'
        }),
        new VueLoaderPlugin()
    ],
    module:{// 配置所有第三方loader 模块的
        rules:[ // 第三方模块的匹配规则
            {test: /\.css$/, use:['style-loader','css-loader']},
            {test: /\.(png|jpg|jpeg|gif|bmp)$/, use: 'url-loader?limit=7631&name=[hash:8]-[name].[ext]'},
            // limit 给定的值，是图片的大小，单位是 byte， 如果我们引用的 图片，大于或等于给定的 limit值，则不会被转为base64格式的字符串， 如果 图片小于给定的 limit 值，则会被转为 base64的字符串
            {test: /\.(ttf|eot|svg|woff|woff2)$/, use:'url-loader'},// 处理 字体文件的 loader
            {test: /\.js$/, use:'babel-loader', exclude:/node_modules/},
            {test: /\.vue$/, use: 'vue-loader'},
            {test: /\.less$/, use: ['style-loader','css-loader','less-loader']}// 处理 less 文件的 loader
        ]
    }
    // resolve:{
    //     alias:{
    //         'vue$':'vue/dist/vue.js'
    //     }
    // }
}